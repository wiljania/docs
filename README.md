# Wiljania
The goal is to make a library for employing a Voluntaric societal platform with
a government and citizens, a place where the laws and regulations would be decided.
The Voluntaric State of Wiljania will be such a place.

## Voluntarism
Voluntarism is a political system intended to implement
a _real direct democracy_.<br>
The system relies on a technology like the Internet,
to have people be able to interact with the system immediately and effortlessly.

### Decentralised Policy Crafting
On an online forum, citizens of the platform will be able
to vote on policies and suggest their own, directly.

A policy is implemented when: `U/D ≥ (P/T)×K`<br>
`U/D` is the ratio of up-votes to down-votes on a policy.<br>
- `P` is the population of the platform.<br>
- `T` is the total number of votes on that policy.<br>
- `K` is a factor determining how prone to change the platform is,
    _i.e. K would be 0.69, would 69% of the platform be a significant majority._<br>

In a clearer way, this could be expressed as:<br>
`Upvotes/Downvotes ≥ (Population/Votes) × RiskOfChange`

_To address concerns regarding the security of e-voting,
blockchains could be implemented, as this has been proven
an extremely safe method of protecting against fraud._

### Funding Distribution
Citizens choose how much of their tax money will be spent in each sector.
Citizens who do not think they know how to spend, all or part of, their money,
will not be required to decide. Their leftover tax money will instead be spent
according to the average distribution.

### The Role of Politicians
Because this does not implement a system in which you vote for representatives,
politicians in the classical sense are not needed.
Anyone can share their voting decisions, their funding distribution, etc., rallying to have you agree with them.
Politicians are not paid by the government, because private political commentators such as Lacy Green, Ben Shapiro, Sargon of Akkad are considered politicians and are paid by their supporters.